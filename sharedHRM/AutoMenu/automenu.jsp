<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>

<html>
	<head>
		<title>HRM Programs Summary</title>
		<link rel="stylesheet" type="text/css" href="sharedHRM/css/css_alternate.css" />
		<link rel="stylesheet" type="text/css" href="css_application.css" />
	</head>
	<body>
		<f:view>
		<f:subview id="pageHeader">
			<h:panelGrid columns="2" width="100%" styleClass="header" cellpadding="0" cellspacing="0">
				<h:outputText id="sysName" value="#{AutoMenuBean.systemName}" 
			  		styleClass="headertext"/>
				<h:outputText id="scrTitle" value="#{AutoMenuBean.sysMenu}" 
			  		styleClass="headertext"/>
			</h:panelGrid>
		</f:subview>
			<h:form id="f_main">
			<h:outputText id="who" value="Logged in as: #{AutoMenuBean.loginID}" styleClass="HRMHeaderDead" />
			  <h:panelGrid columns="1" style="width=100%; height=50%; horizontal-align: middle; vertical-align: middle; text-align: center">
				<h:dataTable border="0" width="100%" style="horizontal-align: middle; vertical-align: middle; text-align: center" 
					rows="0" id="dt1"
					first="0"
					value="#{AutoMenuBean.programTable}" var="program">
					<h:column>
						<h:outputLink value="#{program.progURL}" rendered="#{AutoMenuBean.uiEnabled}">
							<h:outputText value="#{program.programDesc}" styleClass="HRMLink"/>
						</h:outputLink>
							<h:outputText id="ot_desc" value="#{program.programDesc}" styleClass="HRMLock" rendered="#{not AutoMenuBean.uiEnabled}"/>
					</h:column>
					<h:column><f:verbatim> <br /> <br /> </f:verbatim></h:column>
				</h:dataTable>
			  </h:panelGrid>
			  <h:panelGrid columns="2" style="width=100%; text-align: center">
					<h:outputLink id="mnu_back" value="#{AutoMenuBean.caller}">
						<h:graphicImage url="../sharedHRM/images/buttons/backButton.gif" />
					</h:outputLink>
					<h:outputLink id="mnu_top" value="#{AutoMenuBean.topMenu}">
						<h:graphicImage url="../sharedHRM/images/buttons/sysMenu.gif" />
					</h:outputLink>
					<%--<h:outputLink id="mnu_log" value="/Login/login.jsf">
						<h:graphicImage url="../sharedHRM/images/buttons/login.gif" />
					</h:outputLink> --%>
			  </h:panelGrid>
			  <f:verbatim>
			  	  <br/>
			      <HR ALIGN="center" WIDTH="100%" >
			  </f:verbatim>
			  <h:panelGrid columns="2" style="width=100%; horizontal-align: middle; text-align: center">
			  	<h:panelGroup>
			  		<h:outputText value="This text indicates " styleClass="HRMHeaderDead" />
			  		<h:outputText value="Available Options" styleClass="HRMLink"/>
			  	</h:panelGroup>
			  	<h:panelGroup>
			  		<h:outputText value="This text indicates " styleClass="HRMHeaderDead" />
			  		<h:outputText value="No Access" styleClass="HRMLock"/>
			  	</h:panelGroup>
			  </h:panelGrid>
			  <f:verbatim>
			    <HR ALIGN="center" WIDTH="100%" >
			  </f:verbatim>
		</h:form>
		</f:view>
	</body>
</html>
