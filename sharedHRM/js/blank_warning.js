//****************************************************************
// File: blank_warning.js
// Author: Richard Herritt
// Purpose: Function to warn if form field is blank
// Input: strElementName (string) - name of the form element to check
// Output:
//   - Boolean value:
//       false - form element is blank and user does not want to continue
//       true - form element does not exist, or
//              form element exists and has a value, or
//              form element exists, has no value, but user wants to continue anyway
//****************************************************************

function validContactMe(strEmailElementName, strContactMeElementName) {
var objEmailElement, objContactMeElement;
var strAlertMsg = "You must enter an email address if you check the box allowing us to contact you for further details.";
var strWarningMsg = "If you submit this form without an email address you will not be able to use the look up function to follow the progress of your request." +
					" If you still wish to proceed, click 'OK'.";
  
	//End without interruption if no element names are specified
	if (!strEmailElementName || !strContactMeElementName) return true;

	//Find email form element with supplied name
	objEmailElement = findObj(strEmailElementName);

	//Check if the email element value is blank and return boolean
	if (objEmailElement) {
		if (objEmailElement.value == "" || objEmailElement.value == null) {
			//Find ContactMe form element with supplied name
			objContactMeElement = findObj(strContactMeElementName);

			//ContactMe element is found and is checked, warn the user
			if (objContactMeElement && objContactMeElement.checked) {
				//alert (strAlertMsg);
				return false;
			}
			else {
				return confirm(strWarningMsg);
			}
		}
		else {
			//Element is found and has a value, continue without interruption
			return true;
		}
	}
	else {
		//Email element is not found, continue without interruption
		return true;
	}
}

function isEmpty(strElementName) {
var objElement;
var strWarningMsg = "If you submit this form without an email address you will not be able check the progress of your request online. Are you sure?";
  
	//End without interruption if no element name is specified
	if (!strElementName) return true;

	//Find form element with supplied name
	objElement = findObj(strElementName);

	//Check if the element value is blank and return boolean
	if (objElement) {
		if (objElement.value == "" || objElement.value == null) {
			//Element is found and is empty, warn the user
			return confirm(strWarningMsg);
		}
		else {
			//Element is found and has a value, continue without interruption
			return true;
		}
	}
	else {
		//Element is not found, continue without interruption
		return true;
	}
}

