//****************************************************************
// File: form_objects.js
// Author: Richard Herritt
// Purpose: Functions to retrieve form elements for validation
//****************************************************************

function findObj(n, d) 
{ //v4.01
var p,i,j,x;
  
	//Standard code for setting document object
	if (!d) d=document;

	//Custom code for finding fields with invalid names
	for (i=0;!x&&i<d.forms.length;i++) {
		for (j=0;!x&&j<d.forms[i].length;j++) {
			//alert(d.forms[i][j].name + "   and   "  + n);
			if (d.forms[i][j].name == n) {
				x=d.forms[i][j];
			}
		}
	}
  	
	return x;
}

//****************************************************************
// Author: Olena Mitovska
// Purpose: disable forms elements, array of element names is passed as a parameter
//****************************************************************

	
function findObjPartName(n, d) 
{
	var p,i,j,x;
  
	//Standard code for setting document object
	if (!d) d=document;

	//Custom code for finding fields with invalid names
	for (i=0;!x&&i<d.forms.length;i++) {
		for (j=0;!x&&j<d.forms[i].length;j++)
	 {
    
	 			if (d.forms[i][j].name.indexOf(n) >=0) {
				x=d.forms[i][j];
			}
		}
	}
  	
	return x;
}	

/*
 * JSF appends form's name and names of the parental tags to the name of the element
 * when creates plain HTML. So we are looking for partial match of the form's element name
 *
 */
function disableFields()
{
	var argv = disableFields.arguments;
	var argc = argv.length;

	for (i = 0;i < argc ;i++)
    {
		name = argv[i];
		element = findObjPartName(name, null);
      
		if (element)
    		element.disabled = !(element.disabled);
    }//for
	
}//function


/*
 * Adjust the status of text inputs (enabled/disable) with the
 * status of the checkboxes (checked/unchecked) that manage the inputs.
 *
 * @param - name of the controlling element (checkbox)es))
 * @param - array of names of the dependent elements (inputs) 
 * 
 */
function adjustCheckboxInputFields(controlObjName, dependentObjsName, opposite)
{
  var p,i,j,x;
  d = document;
  for (i=0;i<d.forms.length;i++)
  {
    for (j=0;j<d.forms[i].length;j++)
	 {
	    curElement = d.forms[i][j];
	 	if (curElement.name.indexOf(controlObjName) >=0) 
	 	{
 	 		prefix = curElement.name.substring(0, curElement.name.lastIndexOf(controlObjName));
	      	
	 	    for (k = 0;k < dependentObjsName.length ;k++)
    		{
		 	    depObjName = prefix + dependentObjsName[k];
 			 	depObj = document.getElementById(depObjName);
 			 	if (!opposite)
 	 				depObj.disabled = !(curElement.checked);
 	 			else	
 	 				depObj.disabled = (curElement.checked);
 	 		}
		}//if
		
	}//for
   }//for
  
  noenter();
  
}//function

/*
*  Change the status of input (enabled/disabled) AND
*  restore the initial value in it. 
*
*/

function disableRestoreInput(inputName, initialValue)
{
   element = findObjPartName(inputName);
   if (element)
   {
    	element.value = initialValue;	
    	element.disabled = !(element.disabled);
   } 	
}//function

// Return a boolean value telling whether // the first argument is an Array object. 

function isArray()
{

	if (typeof arguments[0] == 'object')
 	{
 	  var criterion =  arguments[0].constructor.toString().match(/array/i); 
	  return (criterion != null); 
	}
	
	return false;
		  
}//function

//Return the province name based on the selected country
// For Canada, we return NS as a province, for all other coutnryies - empty value - OM


function setProvince(obj)
{	
	if (obj)
	{
		provinceObject = findObjPartName("province");
		if (provinceObject)
			if (obj.value == "Canada")
				provinceObject.value="NS";
			else
				provinceObject.value = "";
	}//if

}//function

/**
	Disable form submission by hitting Enter button
*/

function noenter() {
	  /*if (window.event && window.event.keyCode == 13); 
	  	return false;
	  else
	  	return (confirm('If you submit this form anonymously you will not be able check the status of your request online. Are you sure?'))	
		*/ 
		if (document.layers)
			  document.captureEvents(Event.KEYDOWN);
		
		document.onkeydown =
				  function (evt) 
				  {
					    var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) :	event.keyCode;
					    if (keyCode == 13)
					    {
					      return false;
					    }	
				  };
   } 
   
   	/**
	  *  Submit Search form by hitting Enter key
	  *
	  */
	function submitOnEnter(submitButtonId, e)
	{
        var keycode;
        if (window.event) 
                keycode = window.event.keyCode;
        else if (e) 
                keycode = e.which;
        else 
                return true;

        if (keycode == 13) {
                document.getElementById(submitButtonId).click();
                return false;
        } else
                return true;
	}
   
   
   /**
   *  Custom function for Permitting Application
   *
   *  Display comment area under the data table and populate it with the additional information
   *  Based on the passed parameters, locate hidden input element in the table row that contains information
   *  to be displayed in the comment area.
   *
   *  @param id of the link element that was clicked - used to get the number of the row in the table
   *  @param infoType  - can be "comments" or "staff" (employee details)
   *  @param name of style class to be applied to this link
   *
   */
   function showCommentArea(link, infoType, styleClass)
   {

 		element = document.getElementById(link.id.match(/.*:.*[\d]+:/i) + infoType);
		if (element)
		{
		    document.all["commentAreaInner"].innerHTML=element.innerHTML;
			document.all["commentAreaOuter"].style.display='';
	
		} 
		
		changeColor(link, styleClass);	
	}
	
  /**
   *  Custom function for Permitting Application
   *
   *  Dynamically change style of the element.
   *  Keep  reference to the previous element and change its style back
   *  once new element has been passed
   *
   * @param the element
   * @param new style class to be applied 
   *
   */ 
	var oldElement = null;
	var oldStyle = null;
	
	function changeColor(element, selectedElementClassName)
	{	
		if (oldStyle != null && oldElement != null)
		{
			oldElement.className = oldStyle;
		}
				
		oldStyle = element.className;
		oldElement=element;
					
		element.className=selectedElementClassName;
	}
	
	/**
	* The funcitons that opens new windows of the specified size based on the passed parameters
	* and places it in the left bottom corner of the screen
	* Parameters:
	* w - new window's width
	* h - new window's heigth
	* If w or h are blank, the window will open fullsize.
	* URL - the URL of the webpage to load in the new window
	* params - window settigns (toolbar, location bar, etc)
	*/
	
	function openDisplayWin(w, h, URL, params)
	{
				
		if(screen.width && w != null &&  h != null)
		{
			var winL = screen.width-w;
			var winT = screen.height-h;
		}
		else
		{
			winL = 0;
			winT =0;
		}
		
		if (winL < 0) winL = 0;
		if (winT < 0) winT = 0;
	
		if (w == null || w < 0 || h == null || h < 0)
		{
			var settings = 'fullscreen=yes,';
		}
		else
		{
			var settings = 'height=' + h + ',';
			settings += 'width=' + w + ',';
		}
			
		settings += 'left=' + winL + ',';
		settings += 'top=' + winT + ',';
		//if no params passed, create stripped windows
		if (params != null)
			settings += 'toolbar=0,resizable=1,location=0,status=0,scrollbars=1';
		alert("open " + URL);
			myRef = window.open(URL,'newWindow', settings);
	}